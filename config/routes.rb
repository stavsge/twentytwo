Rails.application.routes.draw do
  root "pages#home"
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  resources :users
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :distributors, except: [:destroy]
  resources :in_invoices, path: 'invoices', except: [:destroy] do
    member do
      get :toggle_status
    end
  end
  resources :canteen_reports, except: [:show]
  resources :bank_deposits, except: [:show, :destroy]
end
