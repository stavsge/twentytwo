class CreateCanteenReports < ActiveRecord::Migration[5.1]
  def change
    create_table :canteen_reports do |t|
      t.decimal :field_rentals, precision: 8, scale: 2
      t.decimal :canteen, precision: 8, scale: 2
      t.decimal :coffee, precision: 8, scale: 2
      t.decimal :expenses, precision: 8, scale: 2
      t.integer :z_number
      t.decimal :z_total, precision: 8, scale: 2

      t.timestamps
    end
  end
end
