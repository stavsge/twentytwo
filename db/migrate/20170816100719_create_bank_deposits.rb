class CreateBankDeposits < ActiveRecord::Migration[5.1]
  def change
    create_table :bank_deposits do |t|
      t.datetime :date
      t.decimal :cash, :decimal, precision: 8, scale: 2
      t.decimal :cheque, :decimal, precision: 8, scale: 2

      t.timestamps
    end
  end
end
