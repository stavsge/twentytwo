class AddDateToCanteenReports < ActiveRecord::Migration[5.1]
  def change
    add_column :canteen_reports, :date, :datetime
  end
end
