class CreateInInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :in_invoices do |t|
      t.string :invoice_number
      t.string :description
      t.decimal :amount, precision: 8, scale: 2
      t.integer :status, default: 0
      t.references :distributor, foreign_key: true

      t.timestamps
    end
  end
end
