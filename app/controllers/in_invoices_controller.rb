class InInvoicesController < ApplicationController
  before_action :logged_in_user
  before_action :set_in_invoice, only: [:show, :edit, :update, :toggle_status]

  def index
    @in_invoices = InInvoice.all
  end

  def show

  end

  def new
    @in_invoice = InInvoice.new
  end

  def create
    @in_invoice = InInvoice.new(in_invoice_params)
    if @in_invoice.save
      redirect_to @in_invoice
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @in_invoice.update(in_invoice_params)
      redirect_to @in_invoice
    else
      render :edit
    end
  end

  def toggle_status
    if @in_invoice.pending?
      @in_invoice.paid!
      flash[:success] =  'Invoice status has been Paid'
    else @in_invoice.paid?
    @in_invoice.pending!
    flash[:danger] = 'Invoice status set to Pending'
    end

    redirect_to in_invoice_url
  end


  private
    def set_in_invoice
      @in_invoice = InInvoice.find(params[:id])
    end

    def in_invoice_params
      params.require(:in_invoice).permit(:invoice_number, :description, :amount, :status, :distributor_id)
    end

    def logged_in_user
       unless logged_in? && current_user.admin?
         flash[:danger] = "Only admins can view this page"
         redirect_to root_url
       end
    end
end
