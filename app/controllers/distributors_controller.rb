class DistributorsController < ApplicationController
  before_action :logged_in_user
  before_action :set_distributor, only: [:show, :edit, :update]

  def index
    @distributors = Distributor.all
  end

  def show

  end

  def new
    @distributor = Distributor.new
  end

  def create
    @distributor = Distributor.new(distributor_params)
    if @distributor.save
      redirect_to distributor_path(@distributor)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @distributor.update(distributor_params)
      redirect_to distributor_path(@distributor)
    else
      render :edit
    end
  end

  private

  def set_distributor
    @distributor = Distributor.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def distributor_params
    params.require(:distributor).permit(:name)
  end

  def logged_in_user
     unless logged_in? && current_user.admin?
       flash[:danger] = "Only admins can view this page"
       redirect_to root_url
     end
   end

end
