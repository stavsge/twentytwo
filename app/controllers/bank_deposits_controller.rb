class BankDepositsController < ApplicationController
  before_action :logged_in_user
  before_action :set_bank_deposit, only: [:edit, :update]
  def index
    @deposits = BankDeposit.all
  end

  def new
    @deposit = BankDeposit.new
  end

  def create
    @deposit = BankDeposit.new(bank_deposit_params)
    if @deposit.save
      redirect_to bank_deposits_path
    else
    render :new
    end
  end

  def edit
  end

  def update
    if @deposit.update(canteen_report_params)
      redirect_to bank_deposits_path
    else
      render :edit
    end
  end

  private

  def set_bank_deposit
    @deposit = BankDeposit.find(params[:id])
  end

  def bank_deposit_params
    params.require(:bank_deposit).permit(:date, :cash, :cheque)
  end

  def logged_in_user
     unless logged_in? && current_user.admin?
       flash[:danger] = "Only admins can view this page"
       redirect_to root_url
     end
   end
end
