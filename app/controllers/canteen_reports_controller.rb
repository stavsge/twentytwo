class CanteenReportsController < ApplicationController
  before_action :logged_in_user
  before_action :set_canteen_report, only: [:edit, :update, :destroy]

  def index
    @canteenreports = CanteenReport.all
  end

  def new
    @canteenreport = CanteenReport.new
  end

  def create
    @canteenreport = CanteenReport.new(canteen_report_params)
    if @canteenreport.save
      redirect_to canteen_report_path(@canteenreport)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @canteenreport.update(canteen_report_params)
      redirect_to canteen_report_path(@canteenreport)
    else
      render :edit
    end
  end

  def destroy
    @canteenreport.destroy
    flash[:notice] = "Report deleted"
    redirect_to canteen_reports_path
  end

  private
    def set_canteen_report
      @canteenreport = CanteenReport.find(params[:id])
    end

    def canteen_report_params
      params.require(:canteen_report).permit(:field_rentals, :canteen, :coffee, :expenses, :z_number, :z_total, :date)
    end

    def logged_in_user
       unless logged_in? && current_user.admin?
         flash[:danger] = "Only admins can view this page"
         redirect_to root_url
       end
     end
end
