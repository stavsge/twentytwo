class InInvoice < ApplicationRecord
  belongs_to :distributor
    enum status: { pending: 0, paid: 1 }
  validates_presence_of :description, :invoice_number, :amount
end
