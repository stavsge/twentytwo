class Distributor < ApplicationRecord
  has_many :in_invoices
  validates_presence_of :name
end
