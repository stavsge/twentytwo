class CanteenReport < ApplicationRecord
  validates_presence_of :date, :field_rentals, :canteen, :coffee, :expenses, :z_number, :z_total
end
